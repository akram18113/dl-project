from Datastructures import *
#r(a, b)
#A(b)
#exists r.C (b)
#(exists r.B and exists s.(A and Bottom))(a)

roleAssertion1 = RoleAssertion("r", "a", "b")
conceptAssertion1 = ConceptAssertion(ConceptName("A"),"b")
conceptAssertion2 = ConceptAssertion(ExistentialRoleRestriction("r",ConceptName("C")),"b")
conceptAssertion3 = ConceptAssertion(Conjunction(
	                [ExistentialRoleRestriction
	                     ("r",ConceptName("B")),
	                 ExistentialRoleRestriction
	                     ("s",
	                       Conjunction(["A",ConceptBottom()]))
	                ]),"a")
