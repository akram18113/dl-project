from owlready2 import *


class Concept:
      pass

class ConceptName(Concept):
    def __init__(self,name):
        self.name=name
    def __repr__(self):
        return self.name

class ConceptBottom(Concept):
      def __repr__(self):
        return "Bottom"

class ExistentialRoleRestriction(Concept):
    def __init__(self,role,filler):
        self.role=role
        self.filler=filler
    def __repr__(self):
        return "("+ "exists " +  str(self.role)+"."+str(self.filler)+")"

class Conjunction(Concept):
    def __init__(self,conjuncts):
        self.conjuncts=conjuncts
    def __repr__(self):
        string_list=[]
        for conjunct in self.conjuncts:
            string_list.append(str(conjunct))
        return "("+" and ".join(string_list)+")"

class Axiom:
      pass

class Subsumption(Axiom):
       def __init__(self,concept1,concept2):
        self.concept1=concept1
        self.concept2=concept2

class ConceptAssertion(Axiom):
    def __init__(self,concept,individual):
        self.concept=concept
        self.individual=individual
    def __repr__(self):
        return str(self.concept)+"("+self.individual+")"
        

class RoleAssertion(Axiom):
    def __init__(self,role,individual1,individual2):
        self.role=role
        self.individual1=individual1
        self.individual2=individual2
    def __repr__(self):
        return str(self.role)+"("+self.individual1+","+self.individual2+")"
        

class TBox:
       subsumptions=set()

class ABox:
       conceptAssertions=[]
       roleAssertions=[]
       
